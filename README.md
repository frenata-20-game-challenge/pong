# Pong

## Controls

 * Bottom Player: left/right arrows
 * Top Player: a/d keys
 * Zero/One/Two Player mode: 0/1/2 keys

## Things I'm Happy With

 * cleanly isolated components
   * the ball knows nothing about paddles or scores or players or AI
   * the paddles know nothing about balls or players or AI
   * the AI only knows about the player that it's driving and the ball!
 * full ci-cd automation via gitlab/netlify
 * the ball physics are super simple
 
## Things I'd Like To Improve

 * audio/visual look and feel
 * with the current structure ... triangle pong? pentagon pong?
 * a UI of some sort, even just listing controls
 * more complicated/fallible/predictive AI
 * a way to let players 'shoot' the ball rather than instantiate it in-motion
 
## Deploy

https://bright-licorice-f91cc3.netlify.app/