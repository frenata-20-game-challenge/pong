extends CharacterBody2D

@export var speed: float = 300

# QUESTION: does this need to be physics_process as opposed to process?
func _physics_process(delta):
	if Input.is_action_pressed(get_parent().left):
		velocity = Vector2.LEFT
	elif Input.is_action_pressed(get_parent().right):
		velocity = Vector2.RIGHT
	else:
		velocity = Vector2.ZERO
	move_and_collide(velocity * speed * delta)
