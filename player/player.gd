extends Node2D

signal on_player_score(pos)

# QUESTION: can these be exported as *actions* rather than as strings,
# so that the inspector can cleanly select from the InputMap?
@export var left: String
@export var right: String
	

func _on_score(body):
	body.queue_free()
	on_player_score.emit(get_node("Paddle").position)

func _on_other_player_score(_pos):
	%score.set_text(str(int(%score.get_text())+1))
