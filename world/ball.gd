extends RigidBody2D

@export var speed: float = 100
var trajectory

func rand_direction():
	# should produce 1 or -1
	return 1 if randi()%2 else -1

func _ready():
	# NOTE: I'd prefer to start the ball 'on' a paddle and let the player
	# deliberately shoot it, but I had trouble working out the best way
	# to do so, so for now all balls start in the center with a random
	# trajectory.
	trajectory = Vector2(rand_direction(), rand_direction())

func _physics_process(delta):
	if trajectory.x > 0:
		rotate(.01 * speed * delta)
	elif trajectory.x < 0:
		rotate(-.02 * speed * delta)
		
	var collision = move_and_collide(trajectory * delta * speed)
	if collision:

		# This lets the paddles nudge the ball's left/right velocity.
		var direction = collision.get_collider_velocity().x
		if direction > 0:
			trajectory.x += 1
		elif direction < 0:
			trajectory.x -= 1
		
		# lets a volley start 'easy' and get harder over time
		speed += 10 
		
		# This cleanly bounces the ball off of the paddle or wall.
		trajectory = trajectory.bounce(collision.get_normal())
