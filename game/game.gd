extends Node2D

var ball = preload("res://world/ball.tscn")
var ai_scene   = preload("res://game/ai.tscn")
var ai = {}

func make_ai(target):
	if ai.get(target) == null:
		var a = ai_scene.instantiate()
		a.player = get_node(target)
		ai[target] = a
		call_deferred("add_child", a)

func kill_ai(target):
	var a = ai.get(target)
	if a != null:
		var player = a.player
		a.queue_free()
		Input.action_release(player.left)
		Input.action_release(player.right)
		ai.erase(target)
		
func _input(event):
	if event.is_action_pressed("one-player-game"):
		make_ai("Top Player")
		kill_ai("Bottom Player")
	elif event.is_action_pressed("zero-player-game"):
		make_ai("Top Player")
		make_ai("Bottom Player")
	elif event.is_action_pressed("two-player-game"):
		kill_ai("Top Player")
		kill_ai("Bottom Player")

func _player_score(_pos):
	var new_ball = ball.instantiate()
	
	# place the ball in the center of the viewport
	# of course, the walls and paddles are fixed!, so this breaks
	# on re-size, but this seems preferable to hardcoding more things
	var size = %view.size
	new_ball.position.x = size.x / 2
	new_ball.position.y = size.y / 2
	new_ball.add_to_group("balls")

	call_deferred("add_child", new_ball)
