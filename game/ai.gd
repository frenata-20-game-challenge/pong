extends Node2D

@export var player: Node2D

func _process(delta):
	var ball = get_parent().get_tree().get_first_node_in_group("balls")
	var paddle = player.get_node("%Paddle")
	Input.action_release(player.left)
	Input.action_release(player.right)

	if ball != null:
		# HACK: hardcoded numbers are sadface. Need proper viewports or something!
		# NOTE: rotation shenanigans are necessary because the whole player scene is rotated 180
		var paddle_pos = (1130 - paddle.position.x) if player.rotation else paddle.position.x
		var pos_diff = paddle_pos - ball.position.x
		
		# NOTE: this is where 'more complex' AI scripts would go, possibly including jitter.
		if pos_diff > 5:
			Input.action_press(player.left)
		elif pos_diff < -5:
			Input.action_press(player.right)
		else:
			Input.action_release(player.left)
			Input.action_release(player.right)
