extends Node2D

var time: SceneTreeTimer
var two = preload("res://game/two_pong.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	time = get_tree().create_timer(5)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if time.time_left == 0:
		if get_tree().get_first_node_in_group("level") == null:
			var level = two.instantiate()
			level.add_to_group("level")
			add_child(level)
